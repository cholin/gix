import re
import os
import utils
import tasks

from flask import Flask, render_template, request, jsonify
from werkzeug.contrib.cache import RedisCache
from itertools import islice
from git import Repository, GitObjectRenderer
from markdown import markdown
from pygments import highlight
from pygments.lexers import DiffLexer
from pygments.formatters import HtmlFormatter

try:
    from collections import Counter
except ImportError:
    from python26 import Counter

app = Flask(__name__)
app.config.from_object('settings')

if 'GIX_SETTINGS' in os.environ:
    app.config.from_envvar('GIX_SETTINGS')

cache = RedisCache(key_prefix='cache-')
cache.clear() # remove old locks

app.jinja_env.filters['prettydate'] = utils.prettydate
app.jinja_env.filters['to_date'] = utils.to_date
app.jinja_env.filters['prettybyte'] = utils.humanize_bytes
app.jinja_env.filters['gravatar'] = utils.to_gravatar
app.jinja_env.filters['strip_ref_name'] = utils.strip_ref_name
app.jinja_env.filters['short'] = utils.short
app.jinja_env.filters['commit_subject'] = utils.commit_subject

app.jinja_env.globals['join_paths'] = utils.join_paths
app.jinja_env.globals['back'] = utils.back

@app.route('/git/<reponame>')
@app.route('/git/<reponame>/<revspec>')
@app.route('/git/<reponame>/<revspec>/<path:path>')
def obj(reponame, revspec = 'HEAD', path = None):
    renderer = GitObjectRenderer(reponame, cache, revspec, path)
    return renderer.render_obj()

@app.route('/git/<reponame>/commits')
@app.route('/git/<reponame>/commits/<path:revspec>')
def commits(reponame, revspec = None):
    repo = Repository(reponame)

    if revspec != None:
        rev = repo.revparse_single(revspec)
        formatter = HtmlFormatter(linenos='inline')
        diffs = []
        for parent in rev.parents:
            try:
                data = parent.tree.diff(rev.tree).patch
                diffs.append(highlight(data, DiffLexer(), formatter))
            except:
                pass

        return render_template("objects/commit.html",
                  repo= repo, rev = rev, diffs = diffs)

    log = repo.log(repo.head.target.hex)
    page = int(request.args.get('page', 0))
    revs = islice(log, 20*page, 20*(page+1))
    return render_template("objects/commits.html",
              repo = repo, revs = list(revs), page=page)

@app.route('/git/<reponame>/refs')
def refs(reponame):
    repo = Repository(reponame)
    refs = repo.listall_references()

    branch_ref_id = 'refs/heads/'
    branch_entries = []
    for r in refs:
        if r.startswith(branch_ref_id):
            branch = repo.lookup_reference(r).resolve()
            branch_entries.append((branch, repo[branch.target]))

    def comp(x):
        try:     return int(x[1].commit_time)
        except:  return -1

    branches = sorted(branch_entries, key=comp, reverse=True)

    tag_ref_id = 'refs/tags/'
    tag_entries = []
    for r in refs:
        if r.startswith(tag_ref_id):
            try:
                tag = repo[repo.lookup_reference(r).target]
                tag_entries.append((tag, repo[tag.target]))
            except:
                tag = repo.lookup_reference(r)
                tag_entries.append((tag, repo[tag.target]))
    tags = sorted(tag_entries, key=comp, reverse=True)

    regex = re.compile('refs/.*/pr/.*')
    pulls = []
    for r in refs:
        if regex.match(r):
            pull = repo.lookup_reference(r).resolve()
            pulls.append((pull, repo[pull.target]))
    pull_requests = sorted(pulls, key=comp, reverse=True)


    return render_template("refs.html", repo = repo,
                branches = branches, tags = tags, pull_requests = pull_requests)

@app.route('/git/<reponame>/graphs')
def graphs(reponame):
    repo = Repository(reponame)
    return render_template("graphs.html", repo = repo)

@app.route('/git/<reponame>/readme')
def readme(reponame):
    repo = Repository(reponame)

    try:
        tree = repo[repo.head.target].tree
        blob = repo[tree['README.rst'].oid]
        data = blob.data.decode('utf-8', errors='ignore')
        readme = utils.rst2html(data)
    except KeyError:
        try:
            tree = repo[repo.head.target].tree
            blob = repo[tree['README.md'].oid]
            data = blob.data.decode('utf-8', errors='ignore')
            readme = markdown(data)
        except:
            readme = "No README.rst or README.md found"

    return render_template("readme.html", repo = repo,
              readme = readme)

@app.route('/')
def index():
    def is_valid_repo(path):
        try:
            Repository(path)
        except:
            return False

        return True

    repos = [d for d in os.listdir(app.config['GIT_REPO_BASE']) if is_valid_repo(d)]
    return render_template("index.html", repos = sorted(repos))

@app.route('/api/git/<reponame>/activity')
def api_activity(reponame):
    hit = '{0}-activity'.format(reponame)
    entries = cache.get(hit)
    if entries is None:
        repo = Repository(reponame)
        commits = [x for x in repo.log(repo.head.target.hex)]

        data = [utils.to_date(c.commit_time).strftime("%Y/%m/%d") for c in commits]
        data_sorted = sorted(Counter(data).items())
        entries = [{'date': k, 'commits': v} for k, v in data_sorted]
        cache.set(hit, entries, app.config['CACHE_TIME']);

    return jsonify(activity=entries);

@app.route('/api/git/<reponame>/contributors')
def api_contributors(reponame):
    hit = '{0}-contributors'.format(reponame)
    entries = cache.get(hit)
    if entries is None:
        repo = Repository(reponame)
        commits = [
            (x.author.name) for x in repo.log(repo.head.target.hex)
            if len(x.parents) == 1
        ]
        entries = Counter(commits).items()
        cache.set(hit, entries, app.config['CACHE_TIME']);

    return jsonify(entries);

@app.route('/api/<reponame>/<revspec>/commits', methods=['POST'])
def api_find_commits(reponame, revspec):
    sha_list = request.json['sha_list']

    response = {}
    for sha in reversed(sha_list):
        cache_id = "{0}-commit-{1}".format(reponame, sha)
        entry = cache.get(cache_id)
        if entry is not None:
            response[sha] = entry
            sha_list.remove(sha)

    lock_id = tasks.gen_lock_id(reponame, sha_list)
    if not cache.get(lock_id):
        cache.set(lock_id, app.config['CACHE_TIME'])
        tasks.find_commits.delay(reponame, revspec, sha_list)

    return jsonify(response)


if __name__=="__main__":
    app.run()
