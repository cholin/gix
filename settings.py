import os

GIT_REPO_BASE = os.path.join(os.getenv('HOME'), "projects")
GIT_CLONE_URL_BASE_SSH = 'git://git.spline.de'
GIT_CLONE_URL_BASE_HTTPS = 'https://git.spline.de/git'

HOST = "0.0.0.0"
DEBUG = True
CACHE_TIME = 60*60

