Gix
===

Gix is a pygit2 example application. It's a simple git repository web viewer.
Browse through the code if you interested.

    $ git clone ...
    $ cd gix
    $ pip install -r requirements.txt
    $ # change REPO_BASE in app.py
    $ redis
    $ celery -A tasks worker --loglevel=info
    $ python2 app.py

Live Demo at <http://spielwiese.spline.de>
