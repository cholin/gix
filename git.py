import os
import utils

from flask import render_template, url_for, redirect, current_app
from pygments import highlight
from pygments.lexers import guess_lexer
from pygments.formatters import HtmlFormatter
from pygit2 import Repository, Commit, Tag, Blob, Signature, GIT_SORT_TOPOLOGICAL

class Repository(Repository):

    def __init__(self, name):
        self.name = name
        self.clone_url = {
          'ssh' : '{0}/{1}'.format(
              current_app.config['GIT_CLONE_URL_BASE_SSH'],
              name
          ),
          'https' : '{0}/{1}'.format(
              current_app.config['GIT_CLONE_URL_BASE_HTTPS'],
              name
          ),
        }
        path = os.path.join(current_app.config['GIT_REPO_BASE'], name)
        super(Repository, self).__init__(path)

    def get_name(self):
        return self.name

    def get_clone_url(self, protocol = 'ssh'):
        return self.clone_url[protocol.lower()]

    def log(self, sha, sort = GIT_SORT_TOPOLOGICAL):
        return self.walk(sha, sort)


class GitObjectRenderer:
    def __init__(self, reponame, cache, revspec, path):
        self.repo = Repository(reponame)
        self.revspec = revspec
        self.path = path
        self.object_map = {
            Blob : self._render_blob,
            Commit : self._render_commit,
            Tag : self._render_tag
        }
        self.cache = cache


    def _render_blob(self, rev):
        data = rev.data.decode('utf-8', errors='ignore')
        formatter = HtmlFormatter(linenos='inline')
        lexer = guess_lexer(data)
        blob = highlight(data, lexer, formatter)
        return render_template("objects/blob.html",
                  repo = self.repo,
                  revspec = 'master', blob = blob, path = self.path)

    def _render_commit(self, rev):
        entries = self.repo[rev.tree[self.path].oid] if self.path else rev.tree
        cached = []
        for entry in entries:
            cache_id = "{0}-commit-{1}".format(self.repo.get_name(), entry.hex)
            hit = self.cache.get(cache_id)
            if hit is not None:
                cached.append((entry, hit))
            else:
                cached.append((entry, None))

        files = sorted(cached, key=lambda x: x[0].filemode)
        return render_template("objects/tree.html",
                  repo = self.repo,
                  revspec = self.revspec, rev = rev, files = files,
                  path = self.path)

    def _render_tag(self, rev):
        obj = self.repo[rev.target]
        url = url_for('obj', reponame=self.repo.get_name(), revspec = obj.hex)
        return redirect(url)

    def render_obj(self):
        rev = self.repo.revparse_single(self.revspec)
        return self.object_map[type(rev)](rev)


class GitObjectConverter:
    def __init__(self):
        self.object_map = {
            Commit : self._convert_commit,
            Signature : self._convert_signature
        }

    def _convert_signature(self, signature):
        return  {
          'name' : signature.name,
          'email' : signature.email
        }

    def _convert_commit(self, c):
        return {
          'sha' : c.hex,
          'subject' : utils.truncate(utils.commit_subject(c.message), 60),
          'message' : c.message,
          'commit_time' : utils.prettydate(utils.to_date(c.commit_time)),
          'author' : {
              'name' : c.author.name, 'email' : c.author.email
          },
          'committer' : {
              'name' : c.committer.name, 'email' : c.committer.email
          }
        }

    def convert(self, obj):
        return self.object_map[type(obj)](obj)


