import os
import itertools

from hashlib import md5
from datetime import datetime
from docutils.core import publish_parts
from urllib import urlencode


def humanize_bytes(b, precision=1):
    """
        taken from
        http://code.activestate.com/recipes/577081-humanized-
        representation-of-a-number-of-bytes/
    """

    abbrevs = (
        (1<<50, 'PB'),
        (1<<40, 'TB'),
        (1<<30, 'GB'),
        (1<<20, 'MB'),
        (1<<10, 'kB'),
        (1, 'B')
    )
    if b == 1:
        return '1 B'
    for factor, suffix in abbrevs:
        if b >= factor:
            break
    return '%.*f %s' % (precision, b / factor, suffix)

def to_date(x):
    return datetime.fromtimestamp(x)

def prettydate(d):
    """
      taken from http://stackoverflow.com/a/5164027
    """

    diff = datetime.utcnow() - d
    s = diff.seconds
    if diff.days > 7 or diff.days < 0:
        return d.strftime('%d %b %Y')
    elif diff.days == 1:
        return '1 day ago'
    elif diff.days > 1:
        return '{0} days ago'.format(diff.days)
    elif s <= 1:
        return 'just now'
    elif s < 60:
        return '{0} seconds ago'.format(s)
    elif s < 120:
        return '1 minute ago'
    elif s < 3600:
        return '{0} minutes ago'.format(s/60)
    elif s < 7200:
        return '1 hour ago'
    else:
        return '{0} hours ago'.format(s/3600)

def to_gravatar(email, size=50):
    md5_hash = md5(email.lower()).hexdigest()
    format_str = "http://www.gravatar.com/avatar/{0}.jpg?{1}"
    return format_str.format(md5_hash, urlencode({'s':size}))

def strip_ref_name(ref):
    if any(map(lambda x: x in ref, ['refs/tags/', 'refs/heads/', '/pr/'])):
        return '/'.join(ref.split('/')[2:])
    return ref

def rst2html(data):
    return publish_parts(data, writer_name='html')['html_body']

def short(sha):
    return sha[:7]

def commit_subject(message):
    return message[:message.find("\n\n")]

def find_commits(repo, rev, sha_list):
    commits = []

    walker = repo.log(rev.hex)
    c0 = walker.next()
    for c1 in walker:
        # get all changed files between the last two commits
        files_in_diff = [(x.new_oid, x.old_oid) for x in c1.tree.diff(c0.tree)]

        # filter all files which have been changed
        for e  in reversed(sha_list):
            if e in itertools.chain(*files_in_diff):
              commits.append((e, c0))
              sha_list.remove(e)

        # no files left? break out of iteration
        if len(sha_list) == 0:
            break

        c0 = c1

    for e in sha_list:
        commits.append((e, c0))

    return commits

def join_paths(a, b):
    if a == None:
      return b

    return os.path.join(a, b)

def back(path):
    if path == None or os.sep not in path:
      return None

    path = os.path.split(path)
    length = len(path)
    if length == 2:
      return path[0]

    return os.path.join(path[:length-1])

def truncate(string, size = 20):
    return (string[:size] + '...') if len(string) > size else string
