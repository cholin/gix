import os
import sys
import utils

from hashlib import md5
from celery import Celery
from git import Repository, GitObjectConverter

sys.path.append(os.path.dirname(os.path.basename(__file__)))

celery = Celery('tasks')
celery.config_from_object('celeryconfig')

def gen_lock_id(reponame, sha_list):
    return md5(reponame + str(sha_list)).hexdigest()

@celery.task
def find_commits(reponame, revspec, sha_list):
    from app import app, cache
    with app.test_request_context():
        repo = Repository(reponame)
        rev = repo.revparse_single(revspec)

        commits = utils.find_commits(repo, rev, sha_list)
        converter = GitObjectConverter()
        for sha, c in commits:
            entry = converter.convert(c)
            cache_id = "{0}-commit-{1}".format(repo.get_name(), sha)
            cache.set(cache_id, entry, timeout = app.config['CACHE_TIME'])

        lock_id = gen_lock_id(reponame, sha_list)
        cache.delete(lock_id)
