/**
 * basis graph data structure
 */
  var Node = function(id) {
    if ( id == undefined)
      this.root = true
    else
      this.id = id;
    this.childs = [];
  }

  Node.prototype = {
    'addChild' : function(id) {
      this.childs.push(id);
    },

    'reset' : function() {
      var func = function(node, stack) {
        if (stack.indexOf(node) >= 0)
          return;
        else
          stack.push(node)

        node.visited = false;
        for(var i = 0; i < node.childs.length; i++)
          func(node.childs[i], stack);
      };

      func(this, []);
    }
  }


/**
 * Git graph visulation lib
 */
  var Grip = function(width, height, stepSizeX, stepSizeY) {
    this.width = width;
    this.height = height;
    this.stepSizeX = stepSizeX;
    this.stepSizeY = stepSizeY;
    this.colors = ["#F00", "#FF0", "#0F0", "#0FF", "#00F", "#F0F", "A00",
                   "AA0", "0A0", "0AA", "00A", "AAA"];
    return this;
  }

  Grip.prototype = {
    'parse' : function(revs) {
      var _search = function(id, root) {
        if(root.id == id)
          return root;

        var node;
        for(var i = 0; i < root.childs.length; i++) {
           node = _search(id, root.childs[i]);
           if(node != undefined)
             return node;
        }
      };

      var root = new Node();
      var nodes = [];
      for(var i = 0; i < revs.length; ++i) {
        var rev = revs[i];
        var node = _search(rev.id, root);
        if (node == undefined) {
          node = new Node(rev.id);
          root.addChild(node);
        }

        node.i = i;

        for(var j = 0; j < rev.parents.length; j++) {
          var p = _search(rev.parents[j], root);
          if (p == undefined) {
            p = new Node(rev.parents[j]);
            p.i = revs.length;
          }
          node.addChild(p);
        }

        nodes.push(node);
      }


      this.root = root;
      this.nodes = nodes;
      this._calcLines();
    },

    'getNodes' : function() {
      return this.nodes;
    },

    '_calcLines' : function() {
      var nodes = this.nodes;
      var _func = function(lines, node) {
        if (!node.root) {
          var line = lines[lines.length - 1];

          if (line.length > 0) {
            var current = line[line.length-1];
            var missingNodes = node.i - current.i - 1;
            for ( var i = 1; i <= missingNodes; i++) {
              if (current.i < nodes.length) {
                var tmp = new Node();
                tmp.hidden = true;
                tmp.i = current.i + i;
                line.push(tmp);
              }
            }
          }

          line.push(node);

          if (node.visited)
            return lines;

        }

        node.visited = true;

        for (var i = 0; i < node.childs.length; i++) {
          if (!node.root && i > 0)
            lines.push([node]);

          _func(lines, node.childs[i]);
        }

        return lines;
      }

      this.lines = _func([[]], this.root);
      this.root.reset();

      var intersect = function(node, line) {
        for (var i=1; i < line.length; i++) {
          if (line[i].i == node.i)
            return true
        }
        return false;
      };

      for (var i = 0; i < this.lines.length; i++) {
        var current = this.lines[i];

        for (var j = 0; j < current.length; j++) {
          var node = current[j];
          if (node.visited)
            continue;

          var indent = 0;
          for (var k = 0; k < i; ++k) {
            var compareLine = this.lines[k];
            if (intersect(node, compareLine))
              indent++;
          }

          node.indent = indent;
          node.visited = true;
        }
      }

      this.root.reset();
    },

    'getLines' : function() {
      return this.lines;
    },

    '_getX' : function(node) {
      return this.width - node.indent * this.stepSizeX - 4;
    },

    '_getY' : function(node) {
      return this.stepSizeY * (node.i + 0.5);
    },

    '_isHidden' : function(node) {
      return node.hidden || node.i == this.nodes.length;
    },

    '_getColor' : function(i) {
      var idx =  i % this.colors.length
      return this.colors[idx];
    },

    '_drawLine' : function(ctx, line, color) {
      var n0 = line[0];
      if (!n0.color)
        n0.color = color;

      for (var j = 1; j<line.length; ++j) {
        var n1 = line[j];

        if (!n1.color)
          n1.color = color;

        ctx.strokeStyle = color;
        ctx.beginPath();
        ctx.moveTo(this._getX(n0), this._getY(n0));

        if (Math.abs(this._getX(n0)- this._getX(n1)) > 0) {
          if (Math.abs(this._getY(n0) - this._getY(n1)) > this.stepSizeY) {
            ctx.lineTo(this._getX(n0),this._getY(n1)-this.stepSizeY);
            ctx.bezierCurveTo(
              this._getX(n0), this._getY(n1)-this.stepSizeY/2,
              this._getX(n1), this._getY(n1)-this.stepSizeY/2,
              this._getX(n1), this._getY(n1)
            );
          } else {
            ctx.bezierCurveTo(
              this._getX(n0), this._getY(n0)+this.stepSizeY/2,
              this._getX(n1), this._getY(n1)-this.stepSizeY/2,
              this._getX(n1), this._getY(n1)
            );
          }
        } else {
          ctx.lineTo(this._getX(n1), this._getY(n1));
        }

        ctx.stroke();
        n0 = n1;
      }
    },

    '_drawNode' : function(ctx, node) {
      ctx.beginPath();
      ctx.arc(this._getX(node), this._getY(node), 2.5, 0 , 2 * Math.PI, false);
      ctx.strokeStyle = node.color;
      ctx.fillStyle = node.color;
      ctx.fill();
      ctx.stroke();
    },

    'draw' : function(id) {
      var canvas = document.getElementById("graph");
      canvas.width = this.width;
      canvas.height = this.height;
      var ctx = canvas.getContext("2d");
      ctx.lineWidth = 1;

      var lines = this.getLines();
      for (var i = 0; i < lines.length; i++) {
        var line = lines[i];
        var color = this._getColor(i);

        this._drawLine(ctx, line, color);

        for(var j = 0; j<line.length; ++j) {
          var node = line[j];
          if(!this._isHidden(node))
            this._drawNode(ctx, node);
        }
      }
    }
  }
