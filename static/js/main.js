gix = {
  'doPost' : function(url, data, success) {
    return $.ajax({
      url : url,
      type : "POST",
      data : JSON.stringify(data),
      contentType : "application/json; charset=utf-8",
      dataType : "json",
      success: success}
    );
  },

  'commits' : {
    'update' : function(reponame, url) {
      elements = $('tr[data-sha]');
      shaList = [];
      for (var i = 0; i < elements.length; i++) {
          var elem = $(elements[i]);
          var td = elem.find('td');
          if ($(td[3]).html().trim() == 'Loading data...')
            shaList.push(elem.attr('data-sha'));
      }

      if (shaList.length > 0) {
        var params = { 'sha_list' : shaList };
        gix.doPost(url, params, function(data) {
          var len = 0;
          for (var key in data) {
            var elem = $('[data-sha='+key+'] td');
            var msg = [
              "<a href="+reponame+"/"+data[key]['sha']+">",
              "  "+data[key]['subject'],
              "</a>",
              "<span>",
              "  -",
              "  <a href=\"mailto:"+data[key]['author']['email']+"\">",
              "    "+data[key]['author']['name'],
              "  </a>",
              "</span>",
            ];
            var time = data[key]['commit_time'];

            $(elem[2]).html(time);
            $(elem[3]).html(msg.join('\n'));
            ++len;
          }

          if (len < shaList.length)
            setTimeout(gix.commits.update, 2000, reponame, url);
        });
      }
    }
  }
};
